public class Sender {
    private String name;
    private String surname;
    private String patronimyc;
    private long cardNumber;
    private long accountNumber;
    private int moneyAvailable;

    public Sender(String name, String surname, String patronimyc, int cardNumber, int accountNumber, int moneyAvailable) {
        this.name = name;
        this.surname = surname;
        this.patronimyc = patronimyc;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.moneyAvailable = moneyAvailable;
    }

    public String getName() {
        return surname + " " + name + " " + patronimyc;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

}

