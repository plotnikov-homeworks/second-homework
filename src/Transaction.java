import java.util.Date;

public class Transaction {
    private static int transactionNumber;
    private int amountOfMoney;
    private String currency;
    private Sender sender;
    private Recipient recipient;

    public Transaction(int amountOfMoney, String currency, Sender sender, Recipient recipient) {
        this.amountOfMoney = amountOfMoney;
        this.currency = currency;
        this.sender = sender;
        this.recipient = recipient;
        transactionNumber++;
        System.out.println(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\"Номер перевода\": \"").append(transactionNumber + "\"\n")
                .append("\"Дата\": \"").append(new Date() + "\"\n")
                .append("\"Номер получателя\": \"").append(recipient.getAccountNumber() + "\"\n")
                .append("\"Номер отправителя\": \"").append(sender.getAccountNumber() + "\"\n")
                .append("\"Валюта\": \"").append(currency + "\"\n")
                .append("\"Сумма\": \"").append(amountOfMoney + "\"\n")
                .append("\"Имя получателя\": \"").append(recipient.getName() + "\"\n")
                .append("\"Имя получателя\": \"").append(sender.getName() + "\"\n");
        return builder.toString();
    }
}
